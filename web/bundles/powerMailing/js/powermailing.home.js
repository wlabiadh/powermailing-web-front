$( document ).ready(function() {
    $('input[name="validation"]')[0].setAttribute("required", "true");
});

/**
 * Created by labiadh on 09/09/2015.
 */
$('#postalCode').autocomplete({
    source : function(request, response){
        $.ajax({
            url : '/json/postalCode/search',
            data : {
                postalCode : $('#postalCode').val()
            },
            dataType : 'json',
            type: 'POST',
            success : function(data){
                response($.map(data, function (item){
                    return {
                        label : item.postalCode+", "+item.name,
                        value: item.postalCode
                    }
                }));
            },
            error: function (jqXHR, textStatus, errorThrown){
                console.log(textStatus, errorThrown);
            }
        });
    },
    select : function(event, ui){ // lors de la sélection d'une proposition
        var element = ui.item.label.split(', ');
        $('#city').val(element[1]); // on ajoute la description de l'objet dans un bloc
    }
});
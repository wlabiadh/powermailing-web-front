<?php
/**
 * Created by PhpStorm.
 * User: labiadh
 * Date: 09/09/2015
 * Time: 14:03
 */

namespace PowerMailing\Front\PowerMailingBundle\Helpers;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CityHelper{
    /**
     * @var EntityManager
     */
    protected $em;
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(EntityManager $em, ContainerInterface $container)
    {
        $this->em = $em;
        $this->container = $container;
    }

    private function getCityRepository(){
        return $this->em->getRepository("PowerMailingFrontPowerMailingBundle:City");
    }

    public function getCityByPostalCode($postalCode){
        return $this->getCityRepository()->findOneBy(array('postalCode' => $postalCode));
    }

    public function getCodesPostalLike($characters, $limit = 10){
        $qb = $this->getCityRepository()->createQueryBuilder('c');
        $qb ->select('c.id, c.postalCode', 'c.name')
            ->where('c.postalCode LIKE :characters')
            ->setParameter('characters', $characters.'%')
            ->setMaxResults($limit);

        $result = $qb->getQuery()
            ->getArrayResult();

        $cityList = array();
        foreach($result as $city){
            $cityList[] = array("id"=>$city['id'], "postalCode"=>$city['postalCode'], "name"=>$city['name']);
        }

        return $cityList;
    }
}
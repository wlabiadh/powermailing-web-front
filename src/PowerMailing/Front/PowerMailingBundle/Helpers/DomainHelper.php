<?php
/**
 * Created by PhpStorm.
 * User: labiadh
 * Date: 09/09/2015
 * Time: 14:03
 */

namespace PowerMailing\Front\PowerMailingBundle\Helpers;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use PowerMailing\Front\PowerMailingBundle\Entity\Domain;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DomainHelper{
    /**
     * @var EntityManager
     */
    protected $em;
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(EntityManager $em, ContainerInterface $container)
    {
        $this->em = $em;
        $this->container = $container;
    }

    private function getDomainRepository(){
        return $this->em->getRepository("PowerMailingFrontPowerMailingBundle:Domain");
    }

    public function getAllDomains(){
        return $this->getDomainRepository()->findAll();
    }

    /**
     * @param $domains
     * @return ArrayCollection
     */
    public function getDomainsByIds($domains){
        $list = new ArrayCollection();
        foreach ($domains as $domainId) {
            $domain = $this->getDomainRepository()->find($domainId);
            if($domain instanceof Domain){
                $list->add($domain);
            }
        }
        return $list;
    }
}
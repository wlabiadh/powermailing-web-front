<?php
/**
 * Created by PhpStorm.
 * User: labiadh
 * Date: 09/09/2015
 * Time: 14:03
 */

namespace PowerMailing\Front\PowerMailingBundle\Helpers;

use Doctrine\ORM\EntityManager;
use PowerMailing\Front\PowerMailingBundle\Entity\City;
use PowerMailing\Front\PowerMailingBundle\Entity\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ClientHelper{
    /**
     * @var EntityManager
     */
    protected $em;
    /**
     * @var ContainerInterface
     */
    protected $container;

    const CLIENT_VALIDATION_TEMPLATE = "PowerMailingFrontPowerMailingBundle:template:notification.client.html.twig";

    public function __construct(EntityManager $em, ContainerInterface $container)
    {
        $this->em = $em;
        $this->container = $container;
    }

    private function getClientRepository(){
        return $this->em->getRepository("PowerMailingFrontPowerMailingBundle:Client");
    }

    /**
     * @param $lastName
     * @param $firstName
     * @param $email
     * @param $gender
     * @param $birthday
     * @param $address
     * @param $phone
     * @param $domains
     * @param $city
     */
    public function postClient($lastName, $firstName, $email, $gender, $birthday, $address, $phone, $city, $domains)
    {
        $client = new Client();
        if (!empty($lastName)) {
            $client->setLastName($lastName);
        }

        if (!empty($firstName)) {
            $client->setFirstName($firstName);
        }

        if (!empty($email)) {
            $client->setEmail($email);
        }

        if (!empty($gender)) {
            $client->setGender($gender);
        }

        if (!empty($birthday)) {
            $date = new \DateTime($birthday);
            $client->setBirthday($date);
        }

        if (!empty($address)) {
            $client->setAddress($address);
        }

        if (!empty($phone)) {
            $client->setPhone($phone);
        }

        if ($city instanceof City) {
            $client->setCity($city);
        }

        if(!empty($domains)) {
            $client->setDomains($domains);
        }
        $client->setToken(uniqid());
        $client->setActivated(false);
        $client->setCreated(new \DateTime());

        $this->em->persist($client);
        $this->em->flush();

        $this->sendMail($client);
    }

    private function sendMail($client)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('[Toppromo.fr] Mail de validation')
            ->setFrom('wassim.labiadh@wwsight.com')
            ->setTo($client->getEmail())
            ->setBody(
                $this->container->get('templating')->render(
                    'PowerMailingFrontPowerMailingBundle:template:notification.client.html.twig',
                    array('name' => $client->getFirstName(), 'url' => "http://www.toppromo.fr/app.php/validation?client=".$client->getId().'&token='.$client->getToken())
                ),
                'text/html'
            );
        $this->container->get('mailer')->send($message);
    }

    public function validateEmailClientByIdAndToken($id, $token){
        $client = $this->getClientRepository()->findOneBy(array("id" => $id, "token" => $token));
        if($client instanceof Client){
            $client->setActivated(true);
            $this->em->persist($client);
            $this->em->flush();
            return true;
        }else{
            return false;
        }
    }
}
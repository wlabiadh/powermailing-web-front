<?php

namespace PowerMailing\Front\PowerMailingBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

class DefaultController extends MainController
{
    public function indexAction(Request $request)
    {
        $allDomains = $this->getDomainHelper()->getAllDomains();

        if (!empty($request->get('email'))) {
            $lastName = $request->get('lastName');
            $firstName = $request->get('firstName');
            $email = $request->get('email');
            $gender = $request->get('gender');
            $birthday = $request->get('birthday');
            $address = $request->get('address');
            $postalCode = $request->get('postalCode');
            $phone = $request->get('phone');
            $domainsIds = $request->get('domain');

            $domains = $this->getDomainHelper()->getDomainsByIds($domainsIds);
            $city = $this->getCityHelper()->getCityByPostalCode($postalCode);
            $this->getClientHelper()->postClient($lastName
                                                , $firstName
                                                , $email
                                                , $gender
                                                , $birthday
                                                , $address
                                                , $phone
                                                , $city
                                                , $domains
                                            );
        }
        return $this->render('PowerMailingFrontPowerMailingBundle:Default:index.html.twig', array('allDomains' => $allDomains));
    }

    public function validationAction(Request $request){
        $clientId = $request->get('client');
        $token = $request->get('token');
        $bool = $this->getClientHelper()->validateEmailClientByIdAndToken($clientId, $token);
        return $this->render('PowerMailingFrontPowerMailingBundle:Default:validation.html.twig', array('validation' => $bool));
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: labiadh
 * Date: 09/09/2015
 * Time: 17:13
 */

namespace PowerMailing\Front\PowerMailingBundle\Controller;

use Symfony\Component\HttpFoundation\Response;

class JsonController extends MainController
{
    public function getPostalCodeAction(){
        $request = $this->get('request');
        if($request->isXmlHttpRequest()){
            $characters = $request->request->get('postalCode');
            $cityArray = $this->getCityHelper()->getCodesPostalLike($characters);
            $response = new Response(json_encode($cityArray));
            $response -> headers -> set('Content-Type', 'application/json');
            return $response;
        }
    }
}
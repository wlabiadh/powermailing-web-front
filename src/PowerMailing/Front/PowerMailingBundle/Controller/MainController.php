<?php
/**
 * Created by PhpStorm.
 * User: labiadh
 * Date: 09/09/2015
 * Time: 17:13
 */
namespace PowerMailing\Front\PowerMailingBundle\Controller;

use PowerMailing\Front\PowerMailingBundle\Helpers\CityHelper;
use PowerMailing\Front\PowerMailingBundle\Helpers\ClientHelper;
use PowerMailing\Front\PowerMailingBundle\Helpers\DomainHelper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MainController extends Controller
{
    /**
     * Get Domain helper
     * @return DomainHelper
     */
    protected function getDomainHelper(){
        return $this->container->get('domain.helper');
    }

    /**
     * Get client helper
     * @return ClientHelper
     */
    protected function getClientHelper(){
        return $this->container->get('client.helper');
    }

    /**
     * Get city helper
     * @return CityHelper
     */
    protected function getCityHelper(){
        return $this->container->get('city.helper');
    }
}